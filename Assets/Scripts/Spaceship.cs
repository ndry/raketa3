﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Spaceship : MyMonoBehaviour {
    public new Rigidbody rigidbody { get => GetComponent<Rigidbody>(); }

    public LazyChild<Engine> EngineMain;
    public LazyChild<Engine> EngineTop;
    public LazyChild<Engine> EngineTopLeft;
    public LazyChild<Engine> EngineTopRight;
    public LazyChild<Engine> EngineBottomLeft;
    public LazyChild<Engine> EngineBottomRight;
    void Start() {
        rigidbody.maxAngularVelocity = 20;
    }
}
