﻿#pragma kernel julia

RWTexture2D<float4> textureOut;
RWStructuredBuffer<double> positionDouble;
uniform int2 resolution;
uniform float iterBase;
uniform int iterCount;
uniform bool invert1;
uniform bool mandelbrot;
uniform float4x4 transform;

float3 hue(float h) {
    float r = abs(h * 6 - 3) - 1;
    float g = 2 - abs(h * 6 - 2);
    float b = 2 - abs(h * 6 - 4);
    return saturate(float3(r, g, b));
}

float valueAt(double x, double y, double p, double q) {
	double d = 0;
	int itn = 0;
	while (itn < (iterBase + iterCount) && d <= 256*256){		
		double xnew = x * x - y * y + p;
		double ynew = 2 * x * y + q;
		x = xnew;
		y = ynew;
		d = x * x + y * y;
		itn++;
	}

	float sitn = itn - log2(log2(d)) + 4;
	return sitn;
}

float4 colorFor(float value) {
	if (value > iterBase + iterCount) {
		return float4(0, 0, 0, 1);
	}
	if (value < iterBase - iterCount) {
		return float4(1, 1, 1, 1);

	}
	double c = (iterBase - value) / double(iterCount) * 0.5 + 0.5;
	return float4(hue(c), 1);
}

[numthreads(32,32,1)]
void julia(uint3 id : SV_DispatchThreadID) {
    float4 textureCoords = float4((id.xy - resolution * 0.5), 0, 1);
    float4 coords = mul(transform, textureCoords);

	double cx = positionDouble[0];
	double cy = positionDouble[1];
	double depth = positionDouble[2];

	double coordsX = (coords.x - cx) * depth;
	double coordsY = (coords.y - cy) * depth;

			
	double invx = 1 * coordsX / (coordsX * coordsX + coordsY * coordsY);
	double invy = 1 * -coordsY / (coordsX * coordsX + coordsY * coordsY);

	if (mandelbrot) {
		double p = positionDouble[3];
		double q = positionDouble[4];
		textureOut[id.xy] = colorFor(valueAt(
			invert1 ? invx : coordsX, invert1 ? invy : coordsY,
			p + (invert1 ? invx : coordsX), q + (invert1 ? invy : coordsY)));
	} else {
		double p = positionDouble[3];
		double q = positionDouble[4];
		textureOut[id.xy] = colorFor(valueAt(
			invert1 ? invx : coordsX, invert1 ? invy : coordsY,
			p, q));
	}
}
