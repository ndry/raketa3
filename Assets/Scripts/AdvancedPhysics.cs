﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AdvancedPhysics : MonoBehaviour {
    public JuliaRenderer mainScreen;
    public JuliaRenderer julia1;
    public JuliaRenderer julia2;
    public new Rigidbody rigidbody => GetComponent<Rigidbody>();

    void FixedUpdate() {
        var centerValue = mainScreen.valueAt(transform.position);
        var directions = Enumerable.Range(0, 36)
            .Select(i => Quaternion.Euler(0, 0, i * 360 / 36))
            .Select(q => q * Vector3.up);
        foreach (var direction in directions) {
            var value = mainScreen.valueAt(transform.TransformPoint(direction * 0.01f));
            var v = Math.Pow(value - mainScreen.iterBase, 2);
            var cv = Math.Pow(centerValue - mainScreen.iterBase, 2);
            var dv = v - cv;
            var force = -direction * (float)dv;
            // rigidbody.AddForce(force);
        }

        var dragForce = 0.1f * -rigidbody.velocity * rigidbody.velocity.magnitude;
        // rigidbody.AddForce(dragForce);

        mainScreen.iterBase = centerValue;

        mainScreen.newDepth = 0.0001 * Math.Exp(-(centerValue - 26.15) * 0.3);

        julia1.p = -(mainScreen.cx - mainScreen.transformMatrix.m03) * mainScreen.depth;
        julia1.q = -(mainScreen.cy - mainScreen.transformMatrix.m13) * mainScreen.depth;

        julia2.p = -(mainScreen.cx - mainScreen.transformMatrix.m03) * mainScreen.depth;
        julia2.q = -(mainScreen.cy - mainScreen.transformMatrix.m13) * mainScreen.depth;
    }
}
