﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

[ExecuteAlways]
[RequireComponent(typeof(RawImage))]
public class JuliaRenderer : MonoBehaviour {
    public ComputeShader shader;
    public double depth = 1e-5;
    public double newDepth = 1e-5;
    public double cx = 0, cy = 0;
    public float iterBase = 0;
    public int iterCount = 256;
    public bool invert = false;
    public bool useTransform = true;
    public bool mandelbrot = true;
    public double p;
    public double q;
    public RawImage image => GetComponent<RawImage>();
    public RenderTexture texture {
        get {
            var t = image.texture as RenderTexture;

            if (t && (
                    !t.enableRandomWrite
                    || t.width != image.rectTransform.rect.width
                    || t.height != image.rectTransform.rect.height
            )) {
                image.texture = t = null;
                DestroyImmediate(t);
            }

            if (!t) {
                image.texture
                    = t
                    = new RenderTexture(
                        (int)image.rectTransform.rect.width,
                        (int)image.rectTransform.rect.height,
                        0
                    ) {
                        enableRandomWrite = true
                    };
                t.Create();
                t.filterMode = FilterMode.Point;
            }

            return t;
        }
    }
    public Transform canvas => GetComponentInParent<Canvas>().transform;
    public Matrix4x4 transformMatrix
        => useTransform
            ? canvas.localToWorldMatrix
            : Matrix4x4.identity;
    public void Start() {
        image.texture = null;
    }
    public void FixedUpdate() {
        var tx = transformMatrix.m03;
        var ty = transformMatrix.m13;
        cx = (cx - tx) * depth / newDepth + tx;
        cy = (cy - ty) * depth / newDepth + ty;
        depth = newDepth;
    }
    void Update() {
        RenderShader();
        if (Input.GetKeyUp(KeyCode.Space)) {
            invert = !invert;

            var ccx = (cx - canvas.position.x) * depth;
            var ccy = (cx - canvas.position.y) * depth;
            double invx = 1 * ccx / (ccx * ccx + ccy * ccy);
            double invy = 1 * -ccy / (ccx * ccx + ccy * ccy);

            cx = invx / depth
                + canvas.position.x;
            cy = invy / depth
                + canvas.position.y;
        }
    }

    void RenderShader() {
        if (iterCount < 0) {
            throw new ArgumentOutOfRangeException(nameof(iterCount));
        }

        var juliaKernel = shader.FindKernel("julia");
        shader.SetTexture(juliaKernel, "textureOut", texture);
        var positionData = new[] { cx, cy, depth, p, q };
        var positionBuffer = new ComputeBuffer(positionData.Length, sizeof(double));
        positionBuffer.SetData(positionData);
        shader.SetBuffer(juliaKernel, "positionDouble", positionBuffer);

        shader.SetInts("resolution",
            new int[] { texture.width, texture.height });
        shader.SetInt("iterCount", iterCount);
        shader.SetBool("invert1", invert);
        shader.SetFloat("iterBase", iterBase);
        shader.SetMatrix("transform", transformMatrix);
        shader.SetBool("mandelbrot", mandelbrot);
        shader.Dispatch(juliaKernel,
            Mathf.CeilToInt(texture.width / 32f),
            Mathf.CeilToInt(texture.height / 32f),
            1);

        positionBuffer.Dispose();
    }

    public static float valueAtDouble(double p, double q) {
        double x = p;
        double y = q;
        double d = 0;
        int itn = 0;
        while (itn < 1000 && d < 256 * 256) {
            double xnew = x * x - y * y + p;
            double ynew = 2 * x * y + q;
            x = xnew;
            y = ynew;
            d = x * x + y * y;
            itn++;
        }
        double sitn = itn - Math.Log(Math.Log(d, 2), 2) + 4;
        return (float)sitn;
    }

    public float valueAt(Vector3 pos) {
        double coordsX = (pos.x - cx) * depth;
        double coordsY = (pos.y - cy) * depth;
        double invx = 1 * coordsX / (coordsX * coordsX + coordsY * coordsY);
        double invy = 1 * -coordsY / (coordsX * coordsX + coordsY * coordsY);
        return valueAtDouble(
            invert ? invx : coordsX,
            invert ? invy : coordsY);
    }
}
