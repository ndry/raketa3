﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engine : MyMonoBehaviour
{
    public enum Intensivity { Off, Maneur, Cruise, Forsage }
    
    public static Intensivity IntensivityUp(Intensivity intensivity) {
        switch (intensivity) {
            case Intensivity.Off: {
                return Intensivity.Maneur;
            }
            case Intensivity.Maneur: {
                return Intensivity.Cruise;
            }
            case Intensivity.Cruise: {
                return Intensivity.Forsage;
            }
            case Intensivity.Forsage: {
                throw new ArgumentOutOfRangeException();
            }
            default: {
                throw new NotSupportedException();
            }
        }
    }
    public static Intensivity IntensivityDown(Intensivity intensivity) {
        switch (intensivity) {
            case Intensivity.Off: {
                throw new ArgumentOutOfRangeException();
            }
            case Intensivity.Maneur: {
                return Intensivity.Off;
            }
            case Intensivity.Cruise: {
                return Intensivity.Maneur;
            }
            case Intensivity.Forsage: {
                return Intensivity.Cruise;
            }
            default: {
                throw new NotSupportedException();
            }
        }
    }

    private static float GetIntensivityValue(Intensivity intensivity) {
        switch (intensivity) {
            case Intensivity.Off: {
                return 0f;
            }
            case Intensivity.Maneur: {
                return 0.25f;
            }
            case Intensivity.Cruise: {
                return 1f;
            }
            case Intensivity.Forsage: {
                return 4f;
            }
            default: {
                throw new NotSupportedException();
            }
        }
    }

    public Intensivity intensivity = Intensivity.Off;
    public float power = 500;

    public float GetForceMagnitude(Intensivity intensivity)
        => power * GetIntensivityValue(intensivity);

    public LazyChild<JetAnimation> Jet;
    
    void FixedUpdate()
    {
        if (!GrandparentRigidbody) {
            return;
        }

        GrandparentRigidbody.AddForceAtPosition(
            transform.TransformVector(
                Vector3.up * GetForceMagnitude(intensivity)),
            transform.position);

        Jet.Value.intensivity = GetIntensivityValue(intensivity);
        Jet.Value.gameObject.SetActive(intensivity != Intensivity.Off);

        intensivity = Intensivity.Off;
    }
}
