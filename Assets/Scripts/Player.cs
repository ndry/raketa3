﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MyMonoBehaviour {

    new Rigidbody rigidbody { get => GetComponent<Rigidbody>(); }
    Spaceship spaceship { get => GetComponent<Spaceship>(); }

    Engine.Intensivity GetModifiedIntensivity(
        bool on,
        Engine.Intensivity baseIntensivity
    ) {
        if (!on) {
            return Engine.Intensivity.Off;
        }
        if (Input.GetKey(KeyCode.LeftControl)) {
            baseIntensivity = Engine.IntensivityUp(baseIntensivity);
        }
        if (Input.GetKey(KeyCode.LeftShift)) {
            baseIntensivity = Engine.IntensivityDown(baseIntensivity);
        }
        return baseIntensivity;
    }

    // Update is called once per frame
    void FixedUpdate() {
        // spaceship.EngineTopLeft.Value.intensivity = GetModifiedIntensivity(
        //         Input.GetKey(KeyCode.D)
        //         || Input.GetKey(KeyCode.E)
        //         || Input.GetKey(KeyCode.R),
        //         Engine.Intensivity.Cruise);

        // spaceship.EngineTopRight.Value.intensivity = GetModifiedIntensivity(
        //         Input.GetKey(KeyCode.A)
        //         || Input.GetKey(KeyCode.Q)
        //         || Input.GetKey(KeyCode.T),
        //         Engine.Intensivity.Cruise);

        // spaceship.EngineBottomLeft.Value.intensivity = GetModifiedIntensivity(
        //         Input.GetKey(KeyCode.A)
        //         || Input.GetKey(KeyCode.E)
        //         || Input.GetKey(KeyCode.F),
        //         Engine.Intensivity.Cruise);

        // spaceship.EngineBottomRight.Value.intensivity = GetModifiedIntensivity(
        //         Input.GetKey(KeyCode.D)
        //         || Input.GetKey(KeyCode.Q)
        //         || Input.GetKey(KeyCode.G),
        //         Engine.Intensivity.Cruise);

        // spaceship.EngineTop.Value.intensivity = GetModifiedIntensivity(
        //     Input.GetKey(KeyCode.S),
        //     Engine.Intensivity.Cruise);

        // spaceship.EngineMain.Value.intensivity = Input.GetKey(KeyCode.W)
        //     ? GetModifiedIntensivity(
        //         Input.GetKey(KeyCode.W),
        //         Engine.Intensivity.Maneur)
        //     : GetModifiedIntensivity(
        //         Input.GetKey(KeyCode.Space),
        //         Engine.Intensivity.Cruise);

        rigidbody.velocity = Vector3.zero;
        // if (Input.GetKey(KeyCode.A)) {
        //     rigidbody.velocity += transform.TransformVector(Vector3.left);
        // }
        // if (Input.GetKey(KeyCode.D)) {
        //     rigidbody.velocity += transform.TransformVector(Vector3.right);
        // }
        if (Input.GetKey(KeyCode.W)) {
            rigidbody.velocity += transform.TransformVector(Vector3.up);
        }
        if (Input.GetKey(KeyCode.S)) {
            rigidbody.velocity += transform.TransformVector(Vector3.down);
        }
        rigidbody.velocity *= 10;
        rigidbody.angularVelocity = Vector3.zero;
        if (Input.GetKey(KeyCode.A)) {
            rigidbody.angularVelocity += Vector3.forward;
        }
        if (Input.GetKey(KeyCode.D)) {
            rigidbody.angularVelocity += Vector3.back;
        }
    }
}
