using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MyMonoBehaviour: MonoBehaviour {
    public Rigidbody FindGrandparentRigidbody(Transform t) {
        for (; t; t = t.parent) {
            var r = t.GetComponent<Rigidbody>();
            if (r) {
                return r;
            }
        }
        return null;
    }

    public Rigidbody GrandparentRigidbody {
        get => FindGrandparentRigidbody(transform);
    }

    public MyMonoBehaviour() {
        LazyChild.AssignAll(this);
    }

    public void OnValidate() {
        LazyChild.InvalidateAll(this);
    }

    public IEnumerable<Transform> DirectChildren {
        get {
            for (var i = 0; i < transform.childCount; i++) {
                yield return transform.GetChild(i);
            }
        }
    }

    public IEnumerable<T> GetDirectChildren<T>() where T : Component
        => DirectChildren
            .SelectMany(c => c.GetComponents<T>())
            .Where(c => c);
}